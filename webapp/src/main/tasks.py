#from demoapp.models import Widget
import requests as r
import uuid
from celery import shared_task
from django.conf import settings

IMAGE_URL = "https://unsplash.it/1600/900?random"

@shared_task
def download_image():
    resp = r.get(IMAGE_URL)
    file_ext = resp.headers.get('Content-Type').split('/')[1]
    file_name = settings.BASE_DIR / 'images' / (str(uuid.uuid4())+ "." +file_ext)
    with open(file_name, 'wb') as f:
        for chunk in resp.iter_content(chunk_size=128):
            f.write(chunk)
    return True
