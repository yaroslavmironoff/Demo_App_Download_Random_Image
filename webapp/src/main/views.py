#from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from . import tasks


def home(request):
    tasks.download_image.delay()
    return HttpResponse('<h1>Загрузка случайной картинки</h1>')
